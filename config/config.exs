# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ioquiz,
  ecto_repos: [Ioquiz.Repo]

# Configures the endpoint
config :ioquiz, IoquizWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "32IUCnmL9gm0fafX2Oov8wB0lNgUVsaRN1C7HIPEH7WEutNHz8RgEXEZdx1UiGtX",
  render_errors: [view: IoquizWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Ioquiz.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
