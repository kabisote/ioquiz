defmodule Ioquiz.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Ioquiz.Accounts.Credential


  schema "users" do
    field :birthdate, :date
    field :name, :string
    field :role, :string
    has_one :credential, Credential

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :birthdate, :role])
    |> validate_required([:name, :role])
  end

  def registration_changeset(user, attrs) do
    user
    |> changeset(attrs)
    |> cast_assoc(:credential, with: &Credential.changeset/2, required: true)
  end
end
