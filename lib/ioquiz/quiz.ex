defmodule Ioquiz.Quiz do
  @moduledoc """
  The Quiz context.
  """

  import Ecto.Query, warn: false

  alias Ioquiz.Repo
  alias Ioquiz.Accounts
  alias Ioquiz.Quiz.Lesson
  alias Ioquiz.Quiz.Question
  alias Ioquiz.Quiz.Component


  ###########
  # LESSONS #
  ###########

  @doc """
  Returns the list of lessons.

  ## Examples

      iex> list_lessons()
      [%Lesson{}, ...]

  """
  def list_lessons do
    Lesson
    |> Repo.all()
    |> preload_user()
  end

  def list_user_lessons(%Accounts.User{} = user) do
    Lesson
    |> user_lessons_query(user)
    |> Repo.all()
    |> preload_user()
  end


  @doc """
  Gets a single lesson.

  Raises `Ecto.NoResultsError` if the Lesson does not exist.

  ## Examples

      iex> get_lesson!(123)
      %Lesson{}

      iex> get_lesson!(456)
      ** (Ecto.NoResultsError)

  """
  def get_lesson!(id), do: preload_user(Repo.get!(Lesson, id))

  def get_user_lesson!(%Accounts.User{} = user, id) do
    from(l in Lesson, where: l.id == ^id)
    |> user_lessons_query(user)
    |> Repo.one!()
    |> preload_user()
    |> preload_lesson_questions()
  end


  @doc """
  Creates a lesson.

  ## Examples

      iex> create_lesson(%{field: value})
      {:ok, %Lesson{}}

      iex> create_lesson(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_lesson(%Accounts.User{} = user, attrs \\ %{}) do
    %Lesson{}
    |> Lesson.changeset(attrs)
    |> put_user(user)
    |> Repo.insert()
  end


  @doc """
  Updates a lesson.

  ## Examples

      iex> update_lesson(lesson, %{field: new_value})
      {:ok, %Lesson{}}

      iex> update_lesson(lesson, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_lesson(%Lesson{} = lesson, attrs) do
    lesson
    |> Lesson.changeset(attrs)
    |> Repo.update()
  end


  @doc """
  Deletes a Lesson.

  ## Examples

      iex> delete_lesson(lesson)
      {:ok, %Lesson{}}

      iex> delete_lesson(lesson)
      {:error, %Ecto.Changeset{}}

  """
  def delete_lesson(%Lesson{} = lesson) do
    Repo.delete(lesson)
  end


  @doc """
  Returns an `%Ecto.Changeset{}` for tracking lesson changes.

  ## Examples

      iex> change_lesson(lesson)
      %Ecto.Changeset{source: %Lesson{}}

  """
  def change_lesson(%Accounts.User{} = user, %Lesson{} = lesson) do
    lesson
    |> Lesson.changeset(%{})
    |> put_user(user)
  end



  #############
  # QUESTIONS #
  #############

  @doc """
  Returns the list of questions.

  ## Examples

      iex> list_questions()
      [%Question{}, ...]

  """
  def list_questions do
    Repo.all(Question)
  end

  def list_lesson_questions(%Lesson{} = lesson) do
    Question
    |> lesson_questions_query(lesson)
    |> Repo.all()
    |> preload_lesson()
  end

  
  @doc """
  Gets a single question.

  Raises `Ecto.NoResultsError` if the Question does not exist.

  ## Examples

      iex> get_question!(123)
      %Question{}

      iex> get_question!(456)
      ** (Ecto.NoResultsError)

  """
  def get_question!(id), do: Repo.get!(Question, id)

  def get_lesson_question!(%Lesson{} = lesson, id) do
    from(q in Question, where: q.id == ^id)
    |> lesson_questions_query(lesson)
    |> Repo.one!()
    |> preload_lesson()
    |> preload_components()
  end


  @doc """
  Creates a question.

  ## Examples

      iex> create_question(%{field: value})
      {:ok, %Question{}}

      iex> create_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_question(%Lesson{} = lesson, attrs \\ %{}) do
    %Question{}
    |> Question.changeset(attrs)
    |> put_lesson(lesson)
    |> Repo.insert()
  end


  @doc """
  Updates a question.

  ## Examples

      iex> update_question(question, %{field: new_value})
      {:ok, %Question{}}

      iex> update_question(question, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_question(%Question{} = question, attrs) do
    question
    |> Question.changeset(attrs)
    |> Repo.update()
  end


  @doc """
  Deletes a Question.

  ## Examples

      iex> delete_question(question)
      {:ok, %Question{}}

      iex> delete_question(question)
      {:error, %Ecto.Changeset{}}

  """
  def delete_question(%Question{} = question) do
    Repo.delete(question)
  end


  @doc """
  Returns an `%Ecto.Changeset{}` for tracking question changes.

  ## Examples

      iex> change_question(question)
      %Ecto.Changeset{source: %Question{}}

  """
  def change_question(%Lesson{} = lesson, %Question{} = question) do
    question
    |> Question.changeset(%{})
    |> put_lesson(lesson)
  end
  


  ##############
  # COMPONENTS #
  ##############

  @doc """
  Returns the list of components.

  ## Examples

      iex> list_components()
      [%Component{}, ...]

  """
  def list_components do
    Repo.all(Component)
  end


  @doc """
  Gets a single component.

  Raises `Ecto.NoResultsError` if the Component does not exist.

  ## Examples

      iex> get_component!(123)
      %Component{}

      iex> get_component!(456)
      ** (Ecto.NoResultsError)

  """
  def get_component!(id), do: Repo.get!(Component, id)

  def get_question_component!(%Question{} = question, id) do
    from(c in Component, where: c.id == ^id)
    |> question_components_query(question)
    |> Repo.one!()
    |> preload_question()
  end


  @doc """
  Creates a component.

  ## Examples

      iex> create_component(%{field: value})
      {:ok, %Component{}}

      iex> create_component(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_component(%Question{} = question, attrs \\ %{}) do
    %Component{}
    |> Component.changeset(attrs)
    |> put_question(question)
    |> Repo.insert()
  end


  @doc """
  Updates a component.

  ## Examples

      iex> update_component(component, %{field: new_value})
      {:ok, %Component{}}

      iex> update_component(component, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_component(%Component{} = component, attrs) do
    component
    |> Component.changeset(attrs)
    |> Repo.update()
  end


  @doc """
  Deletes a Component.

  ## Examples

      iex> delete_component(component)
      {:ok, %Component{}}

      iex> delete_component(component)
      {:error, %Ecto.Changeset{}}

  """
  def delete_component(%Component{} = component) do
    Repo.delete(component)
  end


  @doc """
  Returns an `%Ecto.Changeset{}` for tracking component changes.

  ## Examples

      iex> change_component(component)
      %Ecto.Changeset{source: %Component{}}

  """
  def change_component(%Question{} = question, %Component{} = component) do
    component
    |> Component.changeset(%{})
    |> put_question(question)
  end



  ####################
  # HELPER FUNCTIONS #
  ####################
 
  defp put_user(changeset, user) do
    Ecto.Changeset.put_assoc(changeset, :user, user)
  end

  defp put_lesson(changeset, lesson) do
    Ecto.Changeset.put_assoc(changeset, :lesson, lesson)
  end

  defp put_question(changeset, question) do
    Ecto.Changeset.put_assoc(changeset, :question, question)
  end

  defp preload_user(lesson_or_lessons) do
    Repo.preload(lesson_or_lessons, :user)
  end

  defp preload_lesson(question_or_questions) do
    Repo.preload(question_or_questions, :lesson)
  end

  defp preload_lesson_questions(lesson) do
    Repo.preload(lesson, :questions)
  end

  defp preload_question(component_or_components) do
    Repo.preload(component_or_components, :question)
  end

  defp preload_components(question) do
    Repo.preload(question, :components)
  end

  defp user_lessons_query(query, %Accounts.User{id: user_id}) do
    from(l in query, where: l.user_id == ^user_id)
  end

  defp lesson_questions_query(query, %Lesson{id: lesson_id}) do
    from(q in query, where: q.lesson_id == ^lesson_id)
  end

  defp question_components_query(query, %Question{id: question_id}) do
    from(q in query, where: q.question_id == ^question_id)
  end

  alias Ioquiz.Quiz.StudentQuiz

  @doc """
  Returns the list of student_quizzes.

  ## Examples

      iex> list_student_quizzes()
      [%StudentQuiz{}, ...]

  """
  def list_student_quizzes do
    Repo.all(StudentQuiz)
  end

  @doc """
  Gets a single student_quiz.

  Raises `Ecto.NoResultsError` if the Student quiz does not exist.

  ## Examples

      iex> get_student_quiz!(123)
      %StudentQuiz{}

      iex> get_student_quiz!(456)
      ** (Ecto.NoResultsError)

  """
  def get_student_quiz!(id), do: Repo.get!(StudentQuiz, id)

  @doc """
  Creates a student_quiz.

  ## Examples

      iex> create_student_quiz(%{field: value})
      {:ok, %StudentQuiz{}}

      iex> create_student_quiz(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_student_quiz(attrs \\ %{}) do
    %StudentQuiz{}
    |> StudentQuiz.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a student_quiz.

  ## Examples

      iex> update_student_quiz(student_quiz, %{field: new_value})
      {:ok, %StudentQuiz{}}

      iex> update_student_quiz(student_quiz, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_student_quiz(%StudentQuiz{} = student_quiz, attrs) do
    student_quiz
    |> StudentQuiz.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a StudentQuiz.

  ## Examples

      iex> delete_student_quiz(student_quiz)
      {:ok, %StudentQuiz{}}

      iex> delete_student_quiz(student_quiz)
      {:error, %Ecto.Changeset{}}

  """
  def delete_student_quiz(%StudentQuiz{} = student_quiz) do
    Repo.delete(student_quiz)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking student_quiz changes.

  ## Examples

      iex> change_student_quiz(student_quiz)
      %Ecto.Changeset{source: %StudentQuiz{}}

  """
  def change_student_quiz(%StudentQuiz{} = student_quiz) do
    StudentQuiz.changeset(student_quiz, %{})
  end
end
