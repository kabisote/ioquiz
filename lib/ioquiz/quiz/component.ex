defmodule Ioquiz.Quiz.Component do
  use Ecto.Schema
  import Ecto.Changeset


  schema "components" do
    field :content, :string
    field :type, :string
    belongs_to :question, Ioquiz.Quiz.Question

    timestamps()
  end

  @doc false
  def changeset(component, attrs) do
    component
    |> cast(attrs, [:content, :type, :question_id])
    |> validate_required([:content, :type])
  end
end
