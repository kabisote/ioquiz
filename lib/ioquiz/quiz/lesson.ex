defmodule Ioquiz.Quiz.Lesson do
  use Ecto.Schema
  import Ecto.Changeset


  schema "lessons" do
    field :information, :string
    field :title, :string

    belongs_to :user, Ioquiz.Accounts.User
    has_many :questions, Ioquiz.Quiz.Question

    timestamps()
  end

  @doc false
  def changeset(lesson, attrs) do
    lesson
    |> cast(attrs, [:title, :information])
    |> validate_required([:title, :information])
  end
end
