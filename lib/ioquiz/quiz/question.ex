defmodule Ioquiz.Quiz.Question do
  use Ecto.Schema
  import Ecto.Changeset


  schema "questions" do
    field :question, :string
    belongs_to :lesson, Ioquiz.Quiz.Lesson
    has_many :components, Ioquiz.Quiz.Component

    timestamps()
  end

  @doc false
  def changeset(question, attrs) do
    question
    |> cast(attrs, [:question, :lesson_id])
    |> validate_required([:question])
  end
end
