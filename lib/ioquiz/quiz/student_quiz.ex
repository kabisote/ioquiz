defmodule Ioquiz.Quiz.StudentQuiz do
  use Ecto.Schema
  import Ecto.Changeset


  schema "student_quizzes" do
    field :assigned_by, :string
    belongs_to :lesson, Ioquiz.Quiz.Lesson, foreign_key: :quiz_id
    belongs_to :user, Ioquiz.Accounts.User, foreign_key: :student_id

    timestamps()
  end

  @doc false
  def changeset(student_quiz, attrs) do
    student_quiz
    |> cast(attrs, [:student_id, :quiz_id, :assigned_by])
    |> validate_required([:student_id, :quiz_id, :assigned_by])
  end
end
