defmodule Ioquiz.Repo do
  use Ecto.Repo,
    otp_app: :ioquiz,
    adapter: Ecto.Adapters.Postgres
end
