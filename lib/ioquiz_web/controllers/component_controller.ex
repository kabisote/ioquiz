defmodule IoquizWeb.ComponentController do
  use IoquizWeb, :controller

  alias Ioquiz.Quiz
  alias Ioquiz.Quiz.Component

  def new(conn, _params, lesson, question) do
    changeset = Quiz.change_component(question, %Component{})
    render(conn, "new.html", changeset: changeset, lesson: lesson, question: question)
  end

  def create(conn, %{"component" => component_params}, lesson, question) do
    case Quiz.create_component(question, component_params) do
      {:ok, _component} ->
        conn
        |> put_flash(:info, "Component created successfully.")
        |> redirect(to: Routes.lesson_question_path(conn, :show, lesson, question))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}, lesson, question) do
    component = Quiz.get_question_component!(question, id)
    changeset = Quiz.change_component(question, component)
    render(conn, "edit.html", lesson: lesson, question: question, component: component, changeset: changeset)
  end

  def update(conn, %{"id" => id, "component" => component_params}, lesson, question) do
    component = Quiz.get_component!(id)

    case Quiz.update_component(component, component_params) do
      {:ok, component} ->
        conn
        |> put_flash(:info, "Component updated successfully.")
        |> redirect(to: Routes.lesson_question_path(conn, :show, lesson, question))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", component: component, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, lesson, question) do
    component = Quiz.get_component!(id)
    {:ok, _component} = Quiz.delete_component(component)

    conn
    |> put_flash(:info, "Component deleted successfully.")
    |> redirect(to: Routes.lesson_question_path(conn, :show, lesson, question))
  end

  def action(conn, _) do
    lesson = Quiz.get_lesson!(conn.params["lesson_id"])
    question = Quiz.get_question!(conn.params["question_id"])
    args = [conn, conn.params, lesson, question]
    apply(__MODULE__, action_name(conn), args)
  end
end
