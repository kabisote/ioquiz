defmodule IoquizWeb.LessonController do
  use IoquizWeb, :controller

  alias Ioquiz.Quiz
  alias Ioquiz.Quiz.Lesson

  def index(conn, _params, current_user) do
    lessons = Quiz.list_user_lessons(current_user)
    render(conn, "index.html", lessons: lessons)
  end

  def new(conn, _params, current_user) do
    changeset = Quiz.change_lesson(current_user, %Lesson{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"lesson" => lesson_params}, current_user) do
    case Quiz.create_lesson(current_user, lesson_params) do
      {:ok, lesson} ->
        conn
        |> put_flash(:info, "Lesson created successfully.")
        |> redirect(to: Routes.lesson_path(conn, :show, lesson))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}, current_user) do
    lesson = Quiz.get_user_lesson!(current_user, id)
    render(conn, "show.html", lesson: lesson)
  end

  def edit(conn, %{"id" => id}, current_user) do
    lesson = Quiz.get_user_lesson!(current_user, id)
    changeset = Quiz.change_lesson(current_user, lesson)
    render(conn, "edit.html", lesson: lesson, changeset: changeset)
  end

  def update(conn, %{"id" => id, "lesson" => lesson_params}, current_user) do
    lesson = Quiz.get_user_lesson!(current_user, id)

    case Quiz.update_lesson(lesson, lesson_params) do
      {:ok, lesson} ->
        conn
        |> put_flash(:info, "Lesson updated successfully.")
        |> redirect(to: Routes.lesson_path(conn, :show, lesson))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", lesson: lesson, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, current_user) do
    lesson = Quiz.get_user_lesson!(current_user, id)
    {:ok, _lesson} = Quiz.delete_lesson(lesson)

    conn
    |> put_flash(:info, "Lesson deleted successfully.")
    |> redirect(to: Routes.lesson_path(conn, :index))
  end


  def action(conn, _) do
    args = [conn, conn.params, conn.assigns.current_user]
    apply(__MODULE__, action_name(conn), args)
  end
end
