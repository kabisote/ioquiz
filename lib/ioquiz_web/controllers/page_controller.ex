defmodule IoquizWeb.PageController do
  use IoquizWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
