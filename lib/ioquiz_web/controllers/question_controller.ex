defmodule IoquizWeb.QuestionController do
  use IoquizWeb, :controller

  alias Ioquiz.Quiz
  alias Ioquiz.Quiz.Question

  def new(conn, _params, lesson) do
    changeset = Quiz.change_question(lesson, %Question{})
    render(conn, "new.html", changeset: changeset, lesson: lesson)
  end

  def create(conn, %{"question" => question_params}, lesson) do
    case Quiz.create_question(lesson, question_params) do
      {:ok, question} ->
        conn
        |> put_flash(:info, "Question created successfully.")
        |> redirect(to: Routes.lesson_question_path(conn, :show, lesson, question))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, lesson: lesson)
    end
  end

  def show(conn, %{"id" => id}, lesson) do
    question = Quiz.get_lesson_question!(lesson, id)
    IO.inspect question
    render(conn, "show.html", lesson: lesson, question: question)
  end

  def edit(conn, %{"id" => id}, lesson) do
    question = Quiz.get_lesson_question!(lesson, id)
    changeset = Quiz.change_question(lesson, question)
    render(conn, "edit.html", lesson: lesson, question: question, changeset: changeset)
  end

  def update(conn, %{"id" => id, "question" => question_params}, lesson) do
    question = Quiz.get_lesson_question!(lesson, id)

    case Quiz.update_question(question, question_params) do
      {:ok, question} ->
        conn
        |> put_flash(:info, "Question updated successfully.")
        |> redirect(to: Routes.lesson_question_path(conn, :show, lesson, question))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", lesson: lesson, question: question, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, lesson) do
    question = Quiz.get_lesson_question!(lesson, id)
    {:ok, _question} = Quiz.delete_question(question)

    conn
    |> put_flash(:info, "Question deleted successfully.")
    |> redirect(to: Routes.lesson_path(conn, :show, lesson))
  end

  def action(conn, _) do
    lesson = Quiz.get_lesson!(conn.params["lesson_id"])
    args = [conn, conn.params, lesson]
    apply(__MODULE__, action_name(conn), args)
  end
end
