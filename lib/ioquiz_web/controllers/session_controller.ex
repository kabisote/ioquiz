defmodule IoquizWeb.SessionController do
  use IoquizWeb, :controller

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"session" => %{"username" => username, "password" => password}}) do
    case IoquizWeb.Auth.login_by_username_and_password(conn, username, password) do
      {:ok, conn} ->
        conn
        |> put_flash(:info, "Welcome back!")
        |> redirect(to: Routes.page_path(conn, :index))

      {:error, _reason, conn} ->
        conn
        |> put_flash(:error, "Login failed.")
        |> render("new.html")
    end
  end

  def delete(conn, _) do
    conn
    |> IoquizWeb.Auth.logout()
    |> redirect(to: Routes.page_path(conn, :index))
  end
end
