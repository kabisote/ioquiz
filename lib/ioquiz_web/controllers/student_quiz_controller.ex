defmodule IoquizWeb.StudentQuizController do
  use IoquizWeb, :controller

  alias Ioquiz.Quiz
  alias Ioquiz.Quiz.StudentQuiz

  def index(conn, _params) do
    student_quizzes = Quiz.list_student_quizzes()
    render(conn, "index.html", student_quizzes: student_quizzes)
  end

  def new(conn, _params) do
    changeset = Quiz.change_student_quiz(%StudentQuiz{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"student_quiz" => student_quiz_params}) do
    case Quiz.create_student_quiz(student_quiz_params) do
      {:ok, student_quiz} ->
        conn
        |> put_flash(:info, "Student quiz created successfully.")
        |> redirect(to: Routes.student_quiz_path(conn, :show, student_quiz))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    student_quiz = Quiz.get_student_quiz!(id)
    render(conn, "show.html", student_quiz: student_quiz)
  end

  def edit(conn, %{"id" => id}) do
    student_quiz = Quiz.get_student_quiz!(id)
    changeset = Quiz.change_student_quiz(student_quiz)
    render(conn, "edit.html", student_quiz: student_quiz, changeset: changeset)
  end

  def update(conn, %{"id" => id, "student_quiz" => student_quiz_params}) do
    student_quiz = Quiz.get_student_quiz!(id)

    case Quiz.update_student_quiz(student_quiz, student_quiz_params) do
      {:ok, student_quiz} ->
        conn
        |> put_flash(:info, "Student quiz updated successfully.")
        |> redirect(to: Routes.student_quiz_path(conn, :show, student_quiz))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", student_quiz: student_quiz, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    student_quiz = Quiz.get_student_quiz!(id)
    {:ok, _student_quiz} = Quiz.delete_student_quiz(student_quiz)

    conn
    |> put_flash(:info, "Student quiz deleted successfully.")
    |> redirect(to: Routes.student_quiz_path(conn, :index))
  end
end
