defmodule IoquizWeb.Router do
  use IoquizWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug IoquizWeb.Auth
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", IoquizWeb do
    pipe_through :browser

    get "/", PageController, :index

    resources "/users", UserController
    resources "/sessions", SessionController, only: [:new, :create, :delete]
  end

  scope "/manage", IoquizWeb do
    pipe_through [:browser, :authenticate_user]

    resources "/lessons", LessonController do
      resources "/questions", QuestionController, except: [:index] do
        resources "/components", ComponentController, except: [:index]
      end
    end

    resources "/student_quizzes", StudentQuizController
  end

  # Other scopes may use custom stacks.
  # scope "/api", IoquizWeb do
  #   pipe_through :api
  # end
end
