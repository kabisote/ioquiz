defmodule Ioquiz.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string, null: false
      add :username, :string, null: false
      add :birthdate, :date
      add :role, :string, default: "student"
      add :passwordhash, :string, null: false

      timestamps()
    end

    create unique_index(:users, [:username])
  end
end
