defmodule Ioquiz.Repo.Migrations.RemoveUsernameAndPasswordFromUsersTable do
  use Ecto.Migration

  def change do
    alter table(:users) do
      remove :username, :string, null: false
      remove :passwordhash, :string, null: false
    end

    drop_if_exists index(:users, [:username])
  end
end
