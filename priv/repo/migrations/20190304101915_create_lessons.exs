defmodule Ioquiz.Repo.Migrations.CreateLessons do
  use Ecto.Migration

  def change do
    create table(:lessons) do
      add :title, :string
      add :information, :text
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:lessons, [:user_id])
  end
end
