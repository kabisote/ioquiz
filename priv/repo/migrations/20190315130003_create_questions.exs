defmodule Ioquiz.Repo.Migrations.CreateQuestions do
  use Ecto.Migration

  def change do
    create table(:questions) do
      add :question, :text
      add :lesson_id, references(:lessons, on_delete: :delete_all)

      timestamps()
    end

    create index(:questions, [:lesson_id])
  end
end
