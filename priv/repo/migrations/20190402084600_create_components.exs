defmodule Ioquiz.Repo.Migrations.CreateComponents do
  use Ecto.Migration

  def change do
    create table(:components) do
      add :content, :text
      add :type, :string
      add :question_id, references(:questions, on_delete: :delete_all)

      timestamps()
    end

    create index(:components, [:question_id])
  end
end
