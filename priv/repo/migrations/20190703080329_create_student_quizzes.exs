defmodule Ioquiz.Repo.Migrations.CreateStudentQuizzes do
  use Ecto.Migration

  def change do
    create table(:student_quizzes) do
      add :student_id, references(:users)
      add :quiz_id, references(:lessons)
      add :assigned_by, :string

      timestamps()
    end

  end
end
