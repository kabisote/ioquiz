defmodule Ioquiz.QuizTest do
  use Ioquiz.DataCase

  alias Ioquiz.Quiz

  describe "lessons" do
    alias Ioquiz.Quiz.Lesson

    @valid_attrs %{information: "some information", title: "some title"}
    @update_attrs %{information: "some updated information", title: "some updated title"}
    @invalid_attrs %{information: nil, title: nil}

    def lesson_fixture(attrs \\ %{}) do
      {:ok, lesson} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Quiz.create_lesson()

      lesson
    end

    test "list_lessons/0 returns all lessons" do
      lesson = lesson_fixture()
      assert Quiz.list_lessons() == [lesson]
    end

    test "get_lesson!/1 returns the lesson with given id" do
      lesson = lesson_fixture()
      assert Quiz.get_lesson!(lesson.id) == lesson
    end

    test "create_lesson/1 with valid data creates a lesson" do
      assert {:ok, %Lesson{} = lesson} = Quiz.create_lesson(@valid_attrs)
      assert lesson.information == "some information"
      assert lesson.title == "some title"
    end

    test "create_lesson/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Quiz.create_lesson(@invalid_attrs)
    end

    test "update_lesson/2 with valid data updates the lesson" do
      lesson = lesson_fixture()
      assert {:ok, %Lesson{} = lesson} = Quiz.update_lesson(lesson, @update_attrs)
      assert lesson.information == "some updated information"
      assert lesson.title == "some updated title"
    end

    test "update_lesson/2 with invalid data returns error changeset" do
      lesson = lesson_fixture()
      assert {:error, %Ecto.Changeset{}} = Quiz.update_lesson(lesson, @invalid_attrs)
      assert lesson == Quiz.get_lesson!(lesson.id)
    end

    test "delete_lesson/1 deletes the lesson" do
      lesson = lesson_fixture()
      assert {:ok, %Lesson{}} = Quiz.delete_lesson(lesson)
      assert_raise Ecto.NoResultsError, fn -> Quiz.get_lesson!(lesson.id) end
    end

    test "change_lesson/1 returns a lesson changeset" do
      lesson = lesson_fixture()
      assert %Ecto.Changeset{} = Quiz.change_lesson(lesson)
    end
  end

  describe "questions" do
    alias Ioquiz.Quiz.Question

    @valid_attrs %{question: "some question"}
    @update_attrs %{question: "some updated question"}
    @invalid_attrs %{question: nil}

    def question_fixture(attrs \\ %{}) do
      {:ok, question} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Quiz.create_question()

      question
    end

    test "list_questions/0 returns all questions" do
      question = question_fixture()
      assert Quiz.list_questions() == [question]
    end

    test "get_question!/1 returns the question with given id" do
      question = question_fixture()
      assert Quiz.get_question!(question.id) == question
    end

    test "create_question/1 with valid data creates a question" do
      assert {:ok, %Question{} = question} = Quiz.create_question(@valid_attrs)
      assert question.question == "some question"
    end

    test "create_question/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Quiz.create_question(@invalid_attrs)
    end

    test "update_question/2 with valid data updates the question" do
      question = question_fixture()
      assert {:ok, %Question{} = question} = Quiz.update_question(question, @update_attrs)
      assert question.question == "some updated question"
    end

    test "update_question/2 with invalid data returns error changeset" do
      question = question_fixture()
      assert {:error, %Ecto.Changeset{}} = Quiz.update_question(question, @invalid_attrs)
      assert question == Quiz.get_question!(question.id)
    end

    test "delete_question/1 deletes the question" do
      question = question_fixture()
      assert {:ok, %Question{}} = Quiz.delete_question(question)
      assert_raise Ecto.NoResultsError, fn -> Quiz.get_question!(question.id) end
    end

    test "change_question/1 returns a question changeset" do
      question = question_fixture()
      assert %Ecto.Changeset{} = Quiz.change_question(question)
    end
  end

  describe "components" do
    alias Ioquiz.Quiz.Component

    @valid_attrs %{content: "some content", type: "some type"}
    @update_attrs %{content: "some updated content", type: "some updated type"}
    @invalid_attrs %{content: nil, type: nil}

    def component_fixture(attrs \\ %{}) do
      {:ok, component} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Quiz.create_component()

      component
    end

    test "list_components/0 returns all components" do
      component = component_fixture()
      assert Quiz.list_components() == [component]
    end

    test "get_component!/1 returns the component with given id" do
      component = component_fixture()
      assert Quiz.get_component!(component.id) == component
    end

    test "create_component/1 with valid data creates a component" do
      assert {:ok, %Component{} = component} = Quiz.create_component(@valid_attrs)
      assert component.content == "some content"
      assert component.type == "some type"
    end

    test "create_component/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Quiz.create_component(@invalid_attrs)
    end

    test "update_component/2 with valid data updates the component" do
      component = component_fixture()
      assert {:ok, %Component{} = component} = Quiz.update_component(component, @update_attrs)
      assert component.content == "some updated content"
      assert component.type == "some updated type"
    end

    test "update_component/2 with invalid data returns error changeset" do
      component = component_fixture()
      assert {:error, %Ecto.Changeset{}} = Quiz.update_component(component, @invalid_attrs)
      assert component == Quiz.get_component!(component.id)
    end

    test "delete_component/1 deletes the component" do
      component = component_fixture()
      assert {:ok, %Component{}} = Quiz.delete_component(component)
      assert_raise Ecto.NoResultsError, fn -> Quiz.get_component!(component.id) end
    end

    test "change_component/1 returns a component changeset" do
      component = component_fixture()
      assert %Ecto.Changeset{} = Quiz.change_component(component)
    end
  end

  describe "student_quizzes" do
    alias Ioquiz.Quiz.StudentQuiz

    @valid_attrs %{assigned_by: 42, quiz_id: 42, student_id: 42}
    @update_attrs %{assigned_by: 43, quiz_id: 43, student_id: 43}
    @invalid_attrs %{assigned_by: nil, quiz_id: nil, student_id: nil}

    def student_quiz_fixture(attrs \\ %{}) do
      {:ok, student_quiz} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Quiz.create_student_quiz()

      student_quiz
    end

    test "list_student_quizzes/0 returns all student_quizzes" do
      student_quiz = student_quiz_fixture()
      assert Quiz.list_student_quizzes() == [student_quiz]
    end

    test "get_student_quiz!/1 returns the student_quiz with given id" do
      student_quiz = student_quiz_fixture()
      assert Quiz.get_student_quiz!(student_quiz.id) == student_quiz
    end

    test "create_student_quiz/1 with valid data creates a student_quiz" do
      assert {:ok, %StudentQuiz{} = student_quiz} = Quiz.create_student_quiz(@valid_attrs)
      assert student_quiz.assigned_by == 42
      assert student_quiz.quiz_id == 42
      assert student_quiz.student_id == 42
    end

    test "create_student_quiz/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Quiz.create_student_quiz(@invalid_attrs)
    end

    test "update_student_quiz/2 with valid data updates the student_quiz" do
      student_quiz = student_quiz_fixture()
      assert {:ok, %StudentQuiz{} = student_quiz} = Quiz.update_student_quiz(student_quiz, @update_attrs)
      assert student_quiz.assigned_by == 43
      assert student_quiz.quiz_id == 43
      assert student_quiz.student_id == 43
    end

    test "update_student_quiz/2 with invalid data returns error changeset" do
      student_quiz = student_quiz_fixture()
      assert {:error, %Ecto.Changeset{}} = Quiz.update_student_quiz(student_quiz, @invalid_attrs)
      assert student_quiz == Quiz.get_student_quiz!(student_quiz.id)
    end

    test "delete_student_quiz/1 deletes the student_quiz" do
      student_quiz = student_quiz_fixture()
      assert {:ok, %StudentQuiz{}} = Quiz.delete_student_quiz(student_quiz)
      assert_raise Ecto.NoResultsError, fn -> Quiz.get_student_quiz!(student_quiz.id) end
    end

    test "change_student_quiz/1 returns a student_quiz changeset" do
      student_quiz = student_quiz_fixture()
      assert %Ecto.Changeset{} = Quiz.change_student_quiz(student_quiz)
    end
  end
end
