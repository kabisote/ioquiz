defmodule IoquizWeb.StudentQuizControllerTest do
  use IoquizWeb.ConnCase

  alias Ioquiz.Quiz

  @create_attrs %{assigned_by: 42, quiz_id: 42, student_id: 42}
  @update_attrs %{assigned_by: 43, quiz_id: 43, student_id: 43}
  @invalid_attrs %{assigned_by: nil, quiz_id: nil, student_id: nil}

  def fixture(:student_quiz) do
    {:ok, student_quiz} = Quiz.create_student_quiz(@create_attrs)
    student_quiz
  end

  describe "index" do
    test "lists all student_quizzes", %{conn: conn} do
      conn = get(conn, Routes.student_quiz_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Student quizzes"
    end
  end

  describe "new student_quiz" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.student_quiz_path(conn, :new))
      assert html_response(conn, 200) =~ "New Student quiz"
    end
  end

  describe "create student_quiz" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.student_quiz_path(conn, :create), student_quiz: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.student_quiz_path(conn, :show, id)

      conn = get(conn, Routes.student_quiz_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Student quiz"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.student_quiz_path(conn, :create), student_quiz: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Student quiz"
    end
  end

  describe "edit student_quiz" do
    setup [:create_student_quiz]

    test "renders form for editing chosen student_quiz", %{conn: conn, student_quiz: student_quiz} do
      conn = get(conn, Routes.student_quiz_path(conn, :edit, student_quiz))
      assert html_response(conn, 200) =~ "Edit Student quiz"
    end
  end

  describe "update student_quiz" do
    setup [:create_student_quiz]

    test "redirects when data is valid", %{conn: conn, student_quiz: student_quiz} do
      conn = put(conn, Routes.student_quiz_path(conn, :update, student_quiz), student_quiz: @update_attrs)
      assert redirected_to(conn) == Routes.student_quiz_path(conn, :show, student_quiz)

      conn = get(conn, Routes.student_quiz_path(conn, :show, student_quiz))
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, student_quiz: student_quiz} do
      conn = put(conn, Routes.student_quiz_path(conn, :update, student_quiz), student_quiz: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Student quiz"
    end
  end

  describe "delete student_quiz" do
    setup [:create_student_quiz]

    test "deletes chosen student_quiz", %{conn: conn, student_quiz: student_quiz} do
      conn = delete(conn, Routes.student_quiz_path(conn, :delete, student_quiz))
      assert redirected_to(conn) == Routes.student_quiz_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.student_quiz_path(conn, :show, student_quiz))
      end
    end
  end

  defp create_student_quiz(_) do
    student_quiz = fixture(:student_quiz)
    {:ok, student_quiz: student_quiz}
  end
end
